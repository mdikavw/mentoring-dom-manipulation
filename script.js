function addTask(columnId) {
	const column = document.getElementById(columnId);
	const taskInput = column.querySelector('.task-input');

	if (taskInput.value === '') return alert('Input kosong');
	const tasksContainer = column.querySelector('.tasks');
	const newTask = document.createElement('div');
	newTask.className = 'task';
	newTask.innerHTML = taskInput.value;

	const deleteButton = document.createElement('button');
	deleteButton.innerHTML = 'Delete';
	deleteButton.onclick = function () {
		deleteTask(this);
	};

	newTask.appendChild(deleteButton);
	tasksContainer.appendChild(newTask);

	taskInput.value = '';
}

function deleteTask(deleteButton) {
	const task = deleteButton.parentNode;
	const tasksContainer = task.parentNode;
	tasksContainer.removeChild(task);
}
